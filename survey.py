from flask import Flask, json
from flask_cors import CORS
from flask import request
from json import dumps, loads
import random, os
texts=[]
for line in open('all_tweets_uniq.txt'):
    texts.append(line)
kappas=['test kappa 1', 'test kappa 2', 'test kappa 3', 'test kappa 4', 'test kappa 5']
KappaObject={"Kappa":[{"Text":k} for k in kappas]}

api = Flask(__name__)
CORS(api)
def last_ID():
	k=sorted(os.listdir('ids'))
	if len(k)==0:
		return 1
	return k[-1].split('/')[-1]
@api.route('/id', methods=['GET'])
def get_id():
  response=str(int(last_ID())+1)
  return response
@api.route('/random_tweet', methods=['GET'])
def get_random():
  response=json.dumps({"Text":random.choice(texts)})
 # print(response)
  return response
@api.route('/kappa', methods=['GET'])
def get_kappa():
#  print(json.dumps(KappaObject))
  return json.dumps(KappaObject)
@api.route('/submit', methods=['POST'])
def submit():
    JS=request.get_json()
    print(JS)
    id_=JS['id']
    open('ids/'+str(id_),'a+').write(json.dumps(JS))
    return '{"success":1}'
    
   
if __name__ == '__main__':
    api.run(debug=True, port=5000) 
    
