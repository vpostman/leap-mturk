# LEAP-HI MTurk

This project is a website that allows for mturk submissions.
We will create a [linked survey](https://blog.mturk.com/editing-the-survey-link-project-template-in-the-ui-7c75285105fb) where mturkers will get paid to label tweets from the tweat data set. 

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install -r requirements.txt
```

## Usage

```bash
python survey.py
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

---

Author: Kevin Slote \
Email: kslote1@gmail.com \
Author: Kevin Daley \
Email: kmd4669201609@gmail.com
---
